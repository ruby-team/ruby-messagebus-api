From: Balasankar C <balasankarc@autistici.org>
Date: Mon, 28 Dec 2015 09:44:29 +0530
Subject: Port tests to RSpec 3 syntax

---
 Gemfile                                     |   2 +-
 spec/messagebus_ruby_api/messagebus_spec.rb | 102 ++++++++++++++--------------
 2 files changed, 52 insertions(+), 52 deletions(-)

diff --git a/Gemfile b/Gemfile
index 46aea95..97b9301 100644
--- a/Gemfile
+++ b/Gemfile
@@ -3,7 +3,7 @@ source "http://rubygems.org"
 gem 'activesupport'
 
 group :test, :development do
-  gem 'rspec', '2.5.0'
+  gem 'rspec', '>= 3.0.0'
   gem 'rr', '1.0.2'
   gem 'fakeweb'
 end
diff --git a/spec/messagebus_ruby_api/messagebus_spec.rb b/spec/messagebus_ruby_api/messagebus_spec.rb
index c9f7ddc..2fc251c 100644
--- a/spec/messagebus_ruby_api/messagebus_spec.rb
+++ b/spec/messagebus_ruby_api/messagebus_spec.rb
@@ -95,7 +95,7 @@ describe MessagebusApi::Messagebus do
       cert_file_path = File.join(File.dirname(__FILE__), "nofile.pem")
       expect do
         client.cacert_info(cert_file_path)
-      end.should raise_error
+      end.to raise_error
     end
 
     it "accepts a cert file that exists" do
@@ -103,59 +103,59 @@ describe MessagebusApi::Messagebus do
       cert_file_path = File.join(File.dirname(__FILE__), "cacert.pem")
       expect do
         client.cacert_info(cert_file_path)
-      end.should_not raise_error
+      end.not_to raise_error
     end
   end
 
   describe "#add_message" do
     it "buffered send that adds to empty buffer" do
       client.add_message(default_message_params)
-      client.flushed?.should be_false
+      expect(client.flushed?).to be_falsey
     end
 
     it "buffered send that adds to empty buffer and sends with flush_buffer flag" do
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/emails/send", :body => json_valid_send)
       client.add_message(default_message_params, true)
-      client.flushed?.should be_true
+      expect(client.flushed?).to be_truthy
     end
 
     it "should have user-agent and x-messagebus-key set in request headers" do
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/emails/send", :body => json_valid_send)
       client.add_message(default_message_params, true)
-      client.flushed?.should be_true
+      expect(client.flushed?).to be_truthy
 
-      FakeWeb.last_request.get_fields("X-MessageBus-Key").should_not be_nil
-      FakeWeb.last_request.get_fields("User-Agent").should_not be_nil
-      FakeWeb.last_request.get_fields("Content-Type").should_not be_nil
+      expect(FakeWeb.last_request.get_fields("X-MessageBus-Key")).not_to be_nil
+      expect(FakeWeb.last_request.get_fields("User-Agent")).not_to be_nil
+      expect(FakeWeb.last_request.get_fields("Content-Type")).not_to be_nil
     end
 
     it "buffered send that adds to a buffer and auto-flushes" do
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/emails/send", :body => create_success_result(client.message_buffer_size).to_json)
       (client.message_buffer_size-1).times do |idx|
         client.add_message(default_message_params)
-        client.flushed?.should be_false
+        expect(client.flushed?).to be_falsey
       end
       client.add_message(default_message_params)
-      client.flushed?.should be_true
-      client.results[:results].size.should == client.message_buffer_size
+      expect(client.flushed?).to be_truthy
+      expect(client.results[:results].size).to eq(client.message_buffer_size)
     end
 
     it "buffered send that adds templates to a buffer and auto-flushes" do
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/templates/send", :body => create_success_result(client.message_buffer_size).to_json)
       (client.message_buffer_size-1).times do |idx|
         client.add_message(default_template_message_params)
-        client.flushed?.should be_false
+        expect(client.flushed?).to be_falsey
       end
       client.add_message(default_template_message_params)
-      client.flushed?.should be_true
-      client.results[:results].size.should == client.message_buffer_size
+      expect(client.flushed?).to be_truthy
+      expect(client.results[:results].size).to eq(client.message_buffer_size)
     end
   end
 
   describe "#flush" do
     it "flush called on empty buffer" do
       client.flush
-      client.flushed?.should be_false
+      expect(client.flushed?).to be_falsey
     end
 
     it "flush called on partially filled buffer" do
@@ -163,59 +163,59 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/emails/send", :body => create_success_result(message_count).to_json)
       (message_count).times do |idx|
         client.add_message(default_message_params)
-        client.flushed?.should be_false
+        expect(client.flushed?).to be_falsey
       end
       client.flush
-      client.flushed?.should be_true
-      client.results[:results].size.should == message_count
+      expect(client.flushed?).to be_truthy
+      expect(client.results[:results].size).to eq(message_count)
     end
 
     it "doesnt reset connection if under a minute old" do
       current_init_time=client.last_init_time
-      current_init_time.should be > Time.now.utc-5
+      expect(current_init_time).to be > Time.now.utc-5
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/emails/send", :body => create_success_result(1).to_json)
       client.add_message(default_message_params)
       client.flush
-      client.flushed?.should be_true
-      client.results[:results].size.should == 1
-      client.last_init_time.should == current_init_time
+      expect(client.flushed?).to be_truthy
+      expect(client.results[:results].size).to eq(1)
+      expect(client.last_init_time).to eq(current_init_time)
     end
 
     it "resets connection if over a minute old" do
       client.last_init_time=Time.now.utc-60
       current_init_time=client.last_init_time
-      current_init_time.should be < Time.now.utc-59
+      expect(current_init_time).to be < Time.now.utc-59
       FakeWeb.register_uri(:post, "https://api.messagebus.com/api/v3/emails/send", :body => create_success_result(1).to_json)
       client.add_message(default_message_params)
       client.flush
-      client.flushed?.should be_true
-      client.results[:results].size.should == 1
-      client.last_init_time.should be > current_init_time
+      expect(client.flushed?).to be_truthy
+      expect(client.results[:results].size).to eq(1)
+      expect(client.last_init_time).to be > current_init_time
     end
   end
 
   describe "#message_buffer_size=" do
     it "can set the buffer size" do
       client.message_buffer_size=(10)
-      client.message_buffer_size.should == 10
+      expect(client.message_buffer_size).to eq(10)
     end
 
     it "cannot set an invalid buffer size" do
       default_buffer_size = 20
       client.message_buffer_size=(-1)
-      client.message_buffer_size.should == default_buffer_size
+      expect(client.message_buffer_size).to eq(default_buffer_size)
 
       client.message_buffer_size=(0)
-      client.message_buffer_size.should == default_buffer_size
+      expect(client.message_buffer_size).to eq(default_buffer_size)
 
       client.message_buffer_size=(101)
-      client.message_buffer_size.should == default_buffer_size
+      expect(client.message_buffer_size).to eq(default_buffer_size)
 
       client.message_buffer_size=(1)
-      client.message_buffer_size.should == 1
+      expect(client.message_buffer_size).to eq(1)
 
       client.message_buffer_size=(100)
-      client.message_buffer_size.should == 100
+      expect(client.message_buffer_size).to eq(100)
     end
   end
 
@@ -228,9 +228,9 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:get, "https://api.messagebus.com/api/v3/delivery_errors?startDate=#{start_date_str}&endDate=#{end_date_str}&tag=", :body => json_delivery_errors)
       expect do
         response = client.delivery_errors(start_date_str, end_date_str)
-        FakeWeb.last_request.body.should be_nil
-        response.should == json_parse(json_delivery_errors)
-      end.should_not raise_error
+        expect(FakeWeb.last_request.body).to be_nil
+        expect(response).to eq(json_parse(json_delivery_errors))
+      end.not_to raise_error
     end
   end
 
@@ -245,9 +245,9 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:get, expected_request, :body => json_unsubscribes)
       expect do
         response = client.unsubscribes(start_date_str, end_date_str)
-        FakeWeb.last_request.body.should be_nil
-        response.should == json_parse(json_unsubscribes)
-      end.should_not raise_error
+        expect(FakeWeb.last_request.body).to be_nil
+        expect(response).to eq(json_parse(json_unsubscribes))
+      end.not_to raise_error
     end
   end
 
@@ -261,9 +261,9 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:delete, expected_request, :body => json_response_200)
       expect do
         response = client.delete_mailing_list_entry(mailing_list_key, to_email)
-        FakeWeb.last_request.body.should be_nil
-        response[:statusCode].should == 200
-      end.should_not raise_error
+        expect(FakeWeb.last_request.body).to be_nil
+        expect(response[:statusCode]).to eq(200)
+      end.not_to raise_error
 
     end
   end
@@ -277,9 +277,9 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:post, expected_request, :body => json_response_200)
       expect do
         response = client.add_mailing_list_entry(mailing_list_key, merge_fields)
-        FakeWeb.last_request.body.should =~ /mergeField/
-        response[:statusCode].should == 200
-      end.should_not raise_error
+        expect(FakeWeb.last_request.body).to match(/mergeField/)
+        expect(response[:statusCode]).to eq(200)
+      end.not_to raise_error
 
     end
   end
@@ -291,8 +291,8 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:get, expected_request, :body => json_mailing_lists)
       expect do
         response = client.mailing_lists
-        response.should == json_parse(json_mailing_lists)
-      end.should_not raise_error
+        expect(response).to eq(json_parse(json_mailing_lists))
+      end.not_to raise_error
     end
   end
 
@@ -303,8 +303,8 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:post, expected_request, :body => json_mailing_list_create)
       expect do
         response = client.create_mailing_lists("Test List", ["%EMAIL%", "%SOME_TOKEN%"])
-        response.should == json_parse(json_mailing_list_create)
-      end.should_not raise_error
+        expect(response).to eq(json_parse(json_mailing_list_create))
+      end.not_to raise_error
     end
   end
 
@@ -318,14 +318,14 @@ describe MessagebusApi::Messagebus do
       FakeWeb.register_uri(:get, expected_request, :body => json_stats)
       expect do
         response = client.stats(start_date_str, end_date_str)
-        response.should == json_parse(json_stats)
-      end.should_not raise_error
+        expect(response).to eq(json_parse(json_stats))
+      end.not_to raise_error
     end
   end
 
   describe "#format_iso_time" do
     it "formats ISO time in format YYYY-MM-DDTHH:mm:ssZ" do
-      client.format_iso_time(Time.now).should =~ /2\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\dZ/
+      expect(client.format_iso_time(Time.now)).to match(/2\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\dZ/)
     end
   end
 
